module gitlab.com/cloudcreds.io/cloudcreds

go 1.14

require (
	github.com/fatih/color v1.9.0 // indirect
	github.com/golang/protobuf v1.4.2 // indirect
	github.com/hashicorp/go-hclog v0.14.0
	github.com/hashicorp/go-plugin v1.3.0
	github.com/hashicorp/yamux v0.0.0-20190923154419-df201c70410d // indirect
	github.com/mattn/go-colorable v0.1.6 // indirect
	github.com/mitchellh/go-homedir v1.1.0
	github.com/mitchellh/go-testing-interface v1.14.1 // indirect
	github.com/oklog/run v1.1.0 // indirect
	github.com/rs/zerolog v1.18.0
	github.com/spf13/cobra v1.0.0
	github.com/spf13/viper v1.7.0
	gitlab.com/cloudcreds.io/cloudcredentials v0.0.0-20200527055122-231d0b8ed2ae
	golang.org/x/net v0.0.0-20200520182314-0ba52f642ac2 // indirect
	golang.org/x/sys v0.0.0-20200523222454-059865788121 // indirect
	golang.org/x/text v0.3.2 // indirect
	google.golang.org/grpc v1.29.1 // indirect
	google.golang.org/protobuf v1.24.0 // indirect
)
