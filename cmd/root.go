/*
Copyright © 2020 CloudCreds.io

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
  "fmt"
  "github.com/hashicorp/go-plugin"
  "github.com/rs/zerolog"
  "github.com/spf13/cobra"
  "gitlab.com/cloudcreds.io/cloudcredentials/pkg/provider"
  "os"

  homedir "github.com/mitchellh/go-homedir"
  "github.com/spf13/viper"
)


var cfgFile string
var cloudcredsFile string
var logLevel int


// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
  Use:   "cloudcreds",
  Short: "Manage Credentials for Cloud Providers",
  Long: `Cloudcreds is a project to allow for managing the many cloud providers credentials files and methods.`,
  // Uncomment the following line if your bare application
  // has an action associated with it:
  //	Run: func(cmd *cobra.Command, args []string) { },
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
  if err := rootCmd.Execute(); err != nil {
    fmt.Println(err)
    os.Exit(1)
  }
}

func init() {
  cobra.OnInitialize(initConfig)

  // Here you will define your flags and configuration settings.
  // Cobra supports persistent flags, which, if defined here,
  // will be global for your application.

  rootCmd.PersistentFlags().StringVar(&cfgFile, "config", "", "config file (default is $HOME/.cloudcreds.yaml)")
  rootCmd.PersistentFlags().StringVar(&cloudcredsFile,"creds","cloudcreds.json","cloudcreds.json file")
  rootCmd.PersistentFlags().IntVarP(&logLevel,"log-level","l",0,"LogLevel -1 to 5")

  switch logLevel{
  case 5:
    zerolog.SetGlobalLevel(zerolog.PanicLevel)
  case 4:
    zerolog.SetGlobalLevel(zerolog.FatalLevel)
  case 3:
    zerolog.SetGlobalLevel(zerolog.ErrorLevel)
  case 2:
    zerolog.SetGlobalLevel(zerolog.WarnLevel)
  case 1:
    zerolog.SetGlobalLevel(zerolog.InfoLevel)
  case 0:
    zerolog.SetGlobalLevel(zerolog.DebugLevel)
  case -1:
    zerolog.SetGlobalLevel(zerolog.TraceLevel)

  }
}


// initConfig reads in config file and ENV variables if set.
func initConfig() {
  if cfgFile != "" {
    // Use config file from the flag.
    viper.SetConfigFile(cfgFile)
  } else {
    // Find home directory.
    home, err := homedir.Dir()
    if err != nil {
      fmt.Println(err)
      os.Exit(1)
    }

    // Search config in home directory with name ".cloudcreds" (without extension).
    viper.AddConfigPath(home)
    viper.SetConfigName(".cloudcreds")
  }

  viper.AutomaticEnv() // read in environment variables that match

  // If a config file is found, read it in.
  if err := viper.ReadInConfig(); err == nil {
    fmt.Println("Using config file:", viper.ConfigFileUsed())
  }
}

// handshakeConfigs are used to just do a basic handshake between
// a plugin and host. If the handshake fails, a user friendly error is shown.
// This prevents users from executing bad plugins or executing a plugin
// directory. It is a UX feature, not a security feature.
var handshakeConfig = plugin.HandshakeConfig{
  ProtocolVersion:  1,
  MagicCookieKey:   "BASIC_PLUGIN",
  MagicCookieValue: "hello",
}

// pluginMap is the map of plugins we can dispense.
var pluginMap = map[string]plugin.Plugin{
  "cred": &provider.CredentialPersistencePlugin{},
  "auth": &provider.AuthenticationProviderPlugin{},
}

