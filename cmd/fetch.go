/*
Copyright © 2020 CloudCreds.io

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"encoding/json"
	"github.com/hashicorp/go-hclog"
	"github.com/rs/zerolog/log"
	"gitlab.com/cloudcreds.io/cloudcredentials/pkg/provider"
	"os"
	"os/exec"

	"github.com/hashicorp/go-plugin"
	"github.com/spf13/cobra"
)

var fetchForce bool

// fetchCmd represents the fetch command
var fetchCmd = &cobra.Command{
	Use:   "fetch",
	Short: "Get my credentials and create the proper credential files",
	Long:  `Get my credentials and create the proper credential files / environment / whatever `,
	Run: func(cmd *cobra.Command, args []string) {
		path := "./plugins/awscloudcredentialprovider"
		log.Debug().Str("path", path).Msg("Loading CloudCredential provider")

		logger := hclog.New(&hclog.LoggerOptions{
			Name:   "plugin",
			Output: os.Stdout,
			Level:  hclog.Debug,
		})

		client := plugin.NewClient(&plugin.ClientConfig{
			HandshakeConfig: handshakeConfig,
			Plugins:         pluginMap,
			Cmd:             exec.Command(path),
			Logger:          logger,
		})
		defer client.Kill()

		// Connect via RPC
		rpcClient, err := client.Client()
		if err != nil {
			log.Fatal().Err(err)
		}

		// Request the plugin
		raw, err := rpcClient.Dispense("cred")
		if err != nil {
			log.Fatal().Err(err)
		}

		d := Data{
			AccessKey:     "AccessKey",
			SecretKey:     "SecretKey",
			SecurityToken: "SecurityToken",
		}
		c := Credential{Data: d}
		j, _ := json.Marshal(c)
		pi := provider.PersistInput{
			Format: 0,
			Data:   j,
		}

		ccp := raw.(provider.CredentialPersistenceProvider)
		log.Debug().Msg("Persisting")
		_ = ccp.Persist(pi)

	},
}

func init() {
	rootCmd.AddCommand(fetchCmd)
	fetchCmd.PersistentFlags().BoolVarP(&fetchForce, "fetch-all", "a", false, "If set, the providers will be updated every time fetch is called")
}



type Credential struct {
	Data Data `json:"data"`
}

type Data struct {
	AccessKey     string `json:"access_key"`
	SecretKey     string `json:"secret_key"`
	SecurityToken string `json:"security_token"`
}
