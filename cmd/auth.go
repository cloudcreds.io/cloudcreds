/*
Copyright © 2020 CloudCreds.io

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"fmt"
	"github.com/hashicorp/go-hclog"
	"github.com/hashicorp/go-plugin"
	"github.com/rs/zerolog/log"
	"gitlab.com/cloudcreds.io/cloudcredentials/pkg/provider"
	"os"
	"os/exec"

	"github.com/spf13/cobra"
)

var authForce bool

// authCmd represents the auth command
var authCmd = &cobra.Command{
	Use:   "auth",
	Short: "Authenticate with all Authentication providers",
	Long: `authenticate with all Authentication providers (if authentiation is completed by any means,
it will just return (ie.  vault-token is valid and good, or azure auth is complete, etc..)) `,
	Run: func(cmd *cobra.Command, args []string) {
		path := "./plugins/vaultauthenticationprovider"
		log.Debug().Str("path", path).Msg("Loading VaultAuthentication provider")

		logger := hclog.New(&hclog.LoggerOptions{
			Name:   "plugin",
			Output: os.Stdout,
			Level:  hclog.Debug,
		})

		client := plugin.NewClient(&plugin.ClientConfig{
			HandshakeConfig: handshakeConfig,
			Plugins:         pluginMap,
			Cmd:             exec.Command(path),
			Logger:          logger,
		})
		defer client.Kill()

		// Connect via RPC
		rpcClient, err := client.Client()
		if err != nil {
			log.Fatal().Err(err)
		}

		// Request the plugin
		raw, err := rpcClient.Dispense("auth")
		if err != nil {
			log.Fatal().Err(err)
		}
		info := make(map[string]interface{})
		info["BASE_URL"] = "https://vault.cloudcreds.io:8200"
		info["USERNAME"] = "bourne"
		info["PASSWORD"] = "bourne1234"

		ai := provider.AuthenticationProviderInput{
			Method:  "ldap",
			Request: info,
		}
		ap := raw.(provider.AuthenticationProvider)
		log.Debug().Msg("Authenticating")
		token := ap.Authenticate(ai)
		if err != nil {
			log.Fatal().Err(err)
		}
		log.Info().Str("token", fmt.Sprintf("%s", token)).Msg("Authentication Success")

	},
}

func init() {
	rootCmd.AddCommand(authCmd)
	authCmd.PersistentFlags().BoolVarP(&authForce, "force", "f", false, "Force Updating all Authorization Providers")
}
