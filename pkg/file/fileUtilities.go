/*
Copyright © 2020 CloudCreds.io

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package file

import (
	"github.com/rs/zerolog/log"
	"os"
	"path/filepath"
	"strings"
)

//ExpandTilde expands the tilde into the user's home directory
func ExpandTilde(tildePath string) string {
	//make sure the tilde is at the front
	if strings.HasPrefix(tildePath, "~") {
		home, err := os.UserHomeDir()
		if err != nil {
			log.Fatal().Err(err)
		}
		actualPath := filepath.Join(home, tildePath[2:])
		return actualPath
	}
	return tildePath
}

func visitFiles(files *[]string) filepath.WalkFunc {
	return func(path string, info os.FileInfo, err error) error {
		if err != nil {
			log.Fatal().Err(err)
		}
		if !info.IsDir() {
			if e := log.Debug(); e.Enabled() {
				// Compute log output only if enabled.
				e.Str("path", path).Msg("file walker found file")
			}
			*files = append(*files, path)
		}
		return nil
	}
}

//WalkFiles is the same as filepath.Walk except it does not return directories
func WalkFiles(root string) []string {

	var files []string
	err := filepath.Walk(root, visitFiles(&files))
	if err != nil {
		panic(err)
	}

	return files

}
